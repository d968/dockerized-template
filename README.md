# Dockerized Template
My simple template to dockerize stuff.

## Build & Install
```bash
make && sudo make install
```

## License
Copyright 2021 Julian Kahlert

Use of this source code is governed by the MIT
license that can be found in the LICENSE file or at
https://opensource.org/licenses/MIT.
