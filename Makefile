
.DEFAULT_GOAL := default

default:
	./container.build

install:
	./container.build --install

uninstall:
	./container.build --remove

clean:
	rm -rf download
